//
//  APIService.h
//  pocket menu
//
//  Created by AnCheng on 7/31/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIService : NSObject

+ (id)sharedManager;

typedef void(^RequestCompletionHandler)(NSDictionary *result,NSError *error);
typedef void(^RequestCompletionHandler1)(NSArray *result,NSError *error);

- (void)enterLocatinon:(double)lon lat:(double)lat onCompletion:(RequestCompletionHandler)complete;
- (void)getLoactionInfo:(NSString *)locationId onCompletion:(RequestCompletionHandler)complete;
- (void)getProduct:(NSString *)categoryId onCompletion:(RequestCompletionHandler)complete;
- (void)getProductInfo:(NSString *)productId onCompletion:(RequestCompletionHandler)complete;

- (void)leaveLocation:(NSString *)statsId onCompletion:(RequestCompletionHandler)complete;

@end
