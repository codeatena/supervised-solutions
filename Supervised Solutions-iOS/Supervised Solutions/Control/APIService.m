//
//  APIService.m
//  pocket menu
//
//  Created by AnCheng on 7/31/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import "APIService.h"
#import "Constants.h"
#import "AppDelegate.h"

@implementation APIService

+ (id)sharedManager
{
    static APIService *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[super alloc] init];
    });
    return sharedManager;
}

- (void)enterLocatinon:(double)lon lat:(double)lat onCompletion:(RequestCompletionHandler)complete
{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.responseSerializer = responseSerializer;
    
    NSDictionary *params = @{@"task" : @"enterLocation" ,@"lat" : [NSNumber numberWithDouble:lat] , @"lon" : [NSNumber numberWithDouble:lon] ,@"deviceToken" : delegate.deviceToken};
    [manager GET:BASE_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Enter Location: %@", responseObject);
        complete(responseObject ,nil);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        complete(nil ,error);
        
    }];
    
}

- (void)getLoactionInfo:(NSString *)locationId onCompletion:(RequestCompletionHandler)complete
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.responseSerializer = responseSerializer;
    
    NSDictionary *params = @{@"task" : @"getLocationInfo" ,@"location_id" : locationId};
    [manager GET:BASE_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Location: %@", responseObject);
        complete(responseObject ,nil);
    
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        complete(nil ,error);
        
    }];
}

- (void)getProduct:(NSString *)categoryId onCompletion:(RequestCompletionHandler)complete
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.responseSerializer = responseSerializer;
    
    NSDictionary *params = @{@"task" : @"getProduct" ,@"category_id" : categoryId};
    [manager GET:BASE_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Products: %@", responseObject);
        complete(responseObject ,nil);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        complete(nil ,error);
        
    }];
}

- (void)getProductInfo:(NSString *)productId onCompletion:(RequestCompletionHandler)complete
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.responseSerializer = responseSerializer;
    
    NSDictionary *params = @{@"task" : @"getProductDetail" ,@"product_id" : productId};
    [manager GET:BASE_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Product Detail: %@", responseObject);
        complete(responseObject ,nil);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        complete(nil ,error);
        
    }];
}

- (void)leaveLocation:(NSString *)statsId onCompletion:(RequestCompletionHandler)complete
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.responseSerializer = responseSerializer;
    
    NSDictionary *params = @{@"task" : @"leaveLocation" ,@"state_id" : statsId};
    [manager GET:BASE_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Product Detail: %@", responseObject);
        complete(responseObject ,nil);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        complete(nil ,error);
        
    }];
}

@end
