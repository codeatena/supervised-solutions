//
//  SplashViewController.m
//  Supervised Solutions
//
//  Created by User on 03/08/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import "SplashViewController.h"
#import "User.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    [self checkAutoLogin];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (_b_fromMain)
    {
        _b_fromMain = NO;
        [self performSegueWithIdentifier:@"loginSegue" sender:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)checkAutoLogin
{
    NSString *userToken  = [User getUserToken];
    if (userToken == nil)
    {
        [self performSegueWithIdentifier:@"loginSegue" sender:nil];
    }
    else
    {
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            //NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
            
            if (status == AFNetworkReachabilityStatusNotReachable)
            {
                UIAlertView *av = [Global showAlert:@"Error" description:@"Your phone is not connencted to internet. Please check your network status."];
                av.delegate  = self;
                
            }
            else
            {
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
                manager.responseSerializer = responseSerializer;
                NSString *urlStr = [NSString stringWithFormat:@"%@%@?token=%@" , BASE_URL ,TOKEN_VALIDATE_URL ,userToken];
                //NSDictionary *params = @{@"token" : userToken};
                [manager POST:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    NSLog(@"JSON: %@", responseObject);
                    
                    if ([responseObject boolValue])
                    {
                        [self performSegueWithIdentifier:@"mainSegue" sender:nil];
                    }
                    else
                    {
                        [self performSegueWithIdentifier:@"loginSegue" sender:nil];
                    }
                
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    NSLog(@"Error: %@", error);
                    [Global showAlert:@"Error" description:error.localizedDescription];
                    
                }];
            }
        }];
        
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }

}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    exit(0);
}

@end
