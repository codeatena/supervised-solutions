//
//  MainViewController.m
//  Supervised Solutions
//
//  Created by User on 03/08/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import "MainViewController.h"
#import "SplashViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
    
    [User setLogin];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@&token=%@" ,BASE_URL ,APP_URL ,[User getUserToken]];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
    
    if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
       UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:@"information"
                                           message:@"Do you want to turn on Location Services to enable Location Tracking?"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:@"Cancel" ,nil];
        alert.tag = 2;
        [alert show];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    self.navigationController.navigationBarHidden = YES;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doLogout:(id)sender
{
    /*
    [SVProgressHUD dismiss];
    SplashViewController *vc = (SplashViewController *)[self.navigationController.viewControllers objectAtIndex:0];
    vc.b_fromMain = YES;
    [self.navigationController popViewControllerAnimated:NO];
     */
    
    UIAlertView *av = [Global showAlert1:@"information" description:@"Are you sure you want to logout?"];
    av.tag = 1;
    av.delegate = self;
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{

}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"page load finished");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"page load failed");
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==1 && buttonIndex == 0)
    {
        [SVProgressHUD dismiss];
        [User setLogout];
        SplashViewController *vc = (SplashViewController *)[self.navigationController.viewControllers objectAtIndex:0];
        vc.b_fromMain = YES;
        [self.navigationController popViewControllerAnimated:NO];
    }
    
    if (alertView.tag ==2 && buttonIndex == 0)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

@end
