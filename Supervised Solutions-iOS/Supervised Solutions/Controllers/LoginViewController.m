//
//  LoginViewController.m
//  Supervised Solutions
//
//  Created by User on 03/08/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    _usernameTextField.text = @"driver@supervisedsolutions.com.au";
//    _passwordTextField.text = @"password";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doLogin:(id)sender
{
    if (_usernameTextField.text.length == 0 || _passwordTextField.text.length == 0)
    {
        [Global showAlert:@"Error" description:@"Both Username and Password is required"];
        return;
    }
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
    
        if (status == AFNetworkReachabilityStatusNotReachable)
        {
            [Global showAlert:@"Error" description:@"Your phone is not connencted to internet. Please check your network status."];
        }
        else
        {
            [SVProgressHUD showWithStatus:@"please_wait" maskType:SVProgressHUDMaskTypeClear];
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            NSString *urlStr = [NSString stringWithFormat:@"%@%@?username=%@&password=%@" , BASE_URL ,LOGIN_URL ,_usernameTextField.text ,_passwordTextField.text];
            [manager POST:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [SVProgressHUD dismiss];
                NSLog(@"JSON: %@", responseObject);
                
                if ([[responseObject objectForKey:@"Success"] boolValue])
                {
                    [User setUserToken:[responseObject objectForKey:@"UserToken"]];
                    [self performSegueWithIdentifier:@"mainSegue1" sender:nil];
                }
                else
                {
                    [Global showAlert:@"Error" description:[responseObject objectForKey:@"ErrorMessage"]];
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                [SVProgressHUD dismiss];
                NSLog(@"Error: %@", error);
                
                [Global showAlert:@"Error" description:error.localizedDescription];
            }];

        }
        
    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

@end
