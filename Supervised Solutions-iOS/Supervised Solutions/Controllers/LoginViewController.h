//
//  LoginViewController.h
//  Supervised Solutions
//
//  Created by User on 03/08/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (nonatomic ,assign) IBOutlet UITextField *usernameTextField;
@property (nonatomic ,assign) IBOutlet UITextField *passwordTextField;

- (IBAction)doLogin:(id)sender;

@end
