//
//  SplashViewController.h
//  Supervised Solutions
//
//  Created by User on 03/08/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController <UIAlertViewDelegate>

@property (nonatomic) BOOL b_fromMain;

@end
