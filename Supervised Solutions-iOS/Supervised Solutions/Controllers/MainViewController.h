//
//  MainViewController.h
//  Supervised Solutions
//
//  Created by User on 03/08/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UIWebViewDelegate ,UIAlertViewDelegate>

@property (nonatomic ,assign) IBOutlet UIWebView *webView;

- (IBAction)doLogout:(id)sender;

@end
