//
//  User.m
//  Supervised Solutions
//
//  Created by User on 03/08/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import "User.h"

@implementation User

+ (NSString*) getUserToken {
    NSString* userToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"userToken"];
    return userToken;
}

+ (void) setUserToken: (NSString*) userToken {
    [[NSUserDefaults standardUserDefaults] setValue:userToken forKey:@"userToken"];
}

+ (BOOL)isLogin
{
    NSString* isLogin = [[NSUserDefaults standardUserDefaults] stringForKey:@"login"];
    if (isLogin != nil && [isLogin isEqualToString:@"YES"]) return YES;
        return NO;
}

+ (void)setLogin
{
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"login"];
}

+ (void)setLogout
{
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"login"];
}

@end
