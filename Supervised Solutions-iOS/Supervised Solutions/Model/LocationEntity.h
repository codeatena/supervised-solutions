//
//  LocationEntity.h
//  Supervised Solutions
//
//  Created by AnCheng on 8/8/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LocationEntity : NSManagedObject

@property (nonatomic, retain) NSString * lng;
@property (nonatomic, retain) NSString * lat;
@property (nonatomic, retain) NSString * date;

@end
