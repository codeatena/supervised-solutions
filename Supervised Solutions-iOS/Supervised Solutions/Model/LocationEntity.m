//
//  LocationEntity.m
//  Supervised Solutions
//
//  Created by AnCheng on 8/8/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import "LocationEntity.h"


@implementation LocationEntity

@dynamic lng;
@dynamic lat;
@dynamic date;

@end
