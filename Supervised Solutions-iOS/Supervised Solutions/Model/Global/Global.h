//
//  Global.h
//  Supervised Solutions
//
//  Created by AnCheng on 8/8/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Global : NSObject

+ (UIAlertView *)showAlert:(NSString *)title description:(NSString *)description;
+ (UIAlertView *)showAlert1:(NSString *)title description:(NSString *)description;

@end
