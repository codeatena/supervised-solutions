//
//  Constants.h
//  Supervised Solutions
//
//  Created by AnCheng on 8/8/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#ifndef Supervised_Solutions_Constants_h
#define Supervised_Solutions_Constants_h

#define BASE_URL    @"http://supervisedsolutions.azurewebsites.net"

#define TOKEN_VALIDATE_URL @"/api/userapi/preauthenticate"
#define LOGIN_URL          @"/api/userapi/login"
#define APP_URL            @"/account/applogin?systemapptoken=ZDgxNmEzMDUtNzZjMC00NzZjLWIyYjYtMzQwOWRhMGFlNzBm"
#define LOCATION_UPLOAD_URL @"/api/locationapi/?usertoken="

#endif
