//
//  Global.m
//  Supervised Solutions
//
//  Created by AnCheng on 8/8/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import "Global.h"

@implementation Global

+ (UIAlertView *)showAlert:(NSString *)title description:(NSString *)description
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:title message:description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [av show];
    return av;
}

+ (UIAlertView *)showAlert1:(NSString *)title description:(NSString *)description
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:title message:description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
    [av show];
    return av;
}

@end
