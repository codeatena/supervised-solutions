//
//  User.h
//  Supervised Solutions
//
//  Created by User on 03/08/15.
//  Copyright (c) 2015 Alin Hila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

+ (NSString*) getUserToken;
+ (void) setUserToken: (NSString*) userToken;

+ (BOOL)isLogin;
+ (void)setLogin;
+ (void)setLogout;

@end
