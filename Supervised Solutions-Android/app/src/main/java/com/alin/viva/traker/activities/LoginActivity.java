package com.alin.viva.traker.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alin.viva.traker.App;
import com.alin.viva.traker.Consts;
import com.alin.viva.traker.R;
import com.alin.viva.traker.model.User;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import alin.lib.utility.DialogUtility;
import alin.lib.utility.LocationUtility;
import alin.lib.utility.NetworkUtility;
import alin.lib.utility.PreferenceUtility;

public class LoginActivity extends Activity {

    private static final String TAG = "LoginActivity";

    EditText edtUsername;
    EditText edtPassword;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        edtUsername = (EditText) findViewById(R.id.username_editText);
        edtPassword = (EditText) findViewById(R.id.password_editText);
        btnLogin = (Button) findViewById(R.id.login_button);
    }

    private void initValue() {
        edtUsername.setText("driver@supervisedsolutions.com.au");
        edtPassword.setText("password");
    }

    private void initEvent() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login() {
        String username = edtUsername.getText().toString();
        String password = edtPassword.getText().toString();
        if (username.equals("") || password.equals("")) {
            DialogUtility.getInstance().show("Both Username and Password is required");
            return;
        }

        if (!NetworkUtility.getInstance().isNetworkAvailable()) {
            new AlertDialog.Builder(LoginActivity.this)
                .setTitle(R.string.app_name)
                .setMessage("Your phone is not connencted to internet. Please check your network status.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        finish();
                    }
                })
                .show();
        } else {
            final ProgressDialog progressDialog = DialogUtility.getInstance().getProgressDialog(this);
            progressDialog.show();

            String url = Consts.SEVER_ADDRESS + "/api/userapi/login?username=" + username + "&password=" + password;
            Log.e(TAG, "login url: " + url);
            Ion.with(this)
                    .load("post", url)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject jsonObject) {

                            progressDialog.dismiss();

                            if (e != null) {
                                e.printStackTrace();
                            } else {
                                Log.e(TAG, "login result: " + jsonObject.toString());
                                if (jsonObject.get("Success").getAsBoolean()) {
                                    String userToken = jsonObject.get("UserToken").getAsString();
                                    User.setUserToken(userToken);
                                    goLogin();
                                } else {
                                    String msgError = jsonObject.get("ErrorMessage").getAsString();
                                    DialogUtility.getInstance().show(msgError);
                                }
                            }
                        }
                    });
        }
    }

    private void goLogin() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}