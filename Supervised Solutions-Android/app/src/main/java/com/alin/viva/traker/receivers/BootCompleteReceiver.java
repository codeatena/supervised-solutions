package com.alin.viva.traker.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.alin.viva.traker.model.User;
import com.alin.viva.traker.services.LocationService;
import com.alin.viva.traker.services.TimerService;

/**
 * Created by User on 8/4/2015.
 */
public class BootCompleteReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            if (!User.getUserToken().equals("")) {
                context.startService(new Intent(context, LocationService.class));
                context.startService(new Intent(context, TimerService.class));
            }
        }
    }
}