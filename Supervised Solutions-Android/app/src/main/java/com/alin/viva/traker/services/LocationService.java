package com.alin.viva.traker.services;

import android.app.Service;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import alin.lib.utility.LocationUtility;

public class LocationService extends Service implements LocationListener {

    private static final String TAG = "LocationService";

    private boolean currentlyProcessingLocation = false;

    private LocationManager locationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");

        if (!currentlyProcessingLocation) {
            currentlyProcessingLocation = true;
            startTracking();
        }

        return START_STICKY;
    }

    private void startTracking() {
        Log.e(TAG, "startTracking");

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        Location location = null;
        if (LocationUtility.getInstance().isEnableLocationByGPS()) {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } else if (LocationUtility.getInstance().isEnableLocationByNetwork()) {
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if(location != null) {
            onLocationChanged(location);
        }

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onDestroy() {
        stopLocationUpdates();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void stopLocationUpdates() {
        Log.e(TAG, "stopLocationUpdates");
        currentlyProcessingLocation = false;
        locationManager.removeUpdates(this);
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.e(TAG, "track location: " + LocationUtility.getInstance().getLatLng(location));
            LocationUtility.getInstance().setCurrentLocation(location);
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.e(TAG, provider + " " + status);
    }

    public void onProviderEnabled(String provider) {
        Log.e(TAG, provider + " enabled");
    }

    public void onProviderDisabled(String provider) {
        Log.e(TAG, provider + " disabled");
    }
}