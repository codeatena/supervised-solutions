package com.alin.viva.traker.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.alin.viva.traker.Consts;
import com.alin.viva.traker.R;
import com.alin.viva.traker.model.User;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import alin.lib.utility.DialogUtility;
import alin.lib.utility.NetworkUtility;
import alin.lib.utility.PreferenceUtility;

public class SplashActivity extends Activity {

    private static final String TAG = "SplashActivity";
    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {

    }

    private void initValue() {

    }

    private void initEvent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkAutoLogin();
            }
        }, SPLASH_TIME_OUT);
    }

    private void checkAutoLogin() {
        String userToken = User.getUserToken();
        if (userToken.equals("")) {
            goLoginActivity();
        } else {
            if (!NetworkUtility.getInstance().isNetworkAvailable()) {
                new AlertDialog.Builder(SplashActivity.this)
                    .setTitle("Error")
                    .setMessage("Your phone is not connencted to internet. Please check your network status.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            finish();
                        }
                    })
                    .show();
            } else {
                String url = Consts.SEVER_ADDRESS + "/api/userapi/preauthenticate?token=" + userToken;
                Log.e(TAG, "preauthenticate url: " + url);
                Ion.with(this)
                        .load("post", url)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                if (e != null) {
                                    e.printStackTrace();

                                    Log.e(TAG, "Timeout");
                                    DialogUtility.getInstance().show("Timeout");
                                } else {
                                    Log.e(TAG, "preauthenticate result: " + result);
                                    if (result.equals("true")) {
                                        goMainActivity();
                                    }
                                    if (result.equals("false")) {
                                        goLoginActivity();
                                    }
                                }
                            }
                        });
            }
        }
    }

    private void goLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void goMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}