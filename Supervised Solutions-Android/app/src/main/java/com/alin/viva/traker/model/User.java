package com.alin.viva.traker.model;

import alin.lib.utility.PreferenceUtility;

/**
 * Created by User on 7/30/2015.
 */
public class User {
    public static String getUserToken() {
        return PreferenceUtility.getInstance().sharedPreferences.getString("userToken", "");
    }

    public static void setUserToken(String userToken) {
        PreferenceUtility.getInstance().prefEditor.putString("userToken", userToken);
        PreferenceUtility.getInstance().prefEditor.commit();
    }
}