package com.alin.viva.traker.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

import com.alin.viva.traker.Consts;
import com.alin.viva.traker.model.LocationModel;
import com.alin.viva.traker.model.User;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import alin.lib.utility.LocationUtility;
import alin.lib.utility.NetworkUtility;

public class TimerService extends Service {

    private static final String TAG = "TimerService";

    private boolean currentlyProcessingLocation = false;

    TimerTask timerTask = null;
    Timer timer = new Timer();

    final long PERIOD = 5 * 60 * 1000;

    public TimerService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.e(TAG, "onStartCommand");

        if (!currentlyProcessingLocation) {
            currentlyProcessingLocation = true;
            startTimer();
        }

        return START_STICKY;
    }

    private void startTimer() {
        stopTimer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
//                if (LocationUtility.getInstance().isEnableLocation()) {
                    Location location = LocationUtility.getInstance().getCurrentLocation();
                    LocationModel locationModel = new LocationModel(location);
                    Log.e(TAG, "get current location: " + locationModel.getInf());
                    LocationModel.addRecord(locationModel);
                    uploadLocations();
//                }
            }
        };
        timer = new Timer();
        timer.schedule(timerTask, PERIOD, PERIOD);
    }

    private void uploadLocations() {
        ArrayList<LocationModel> arrLocationModels = LocationModel.getAllRecords();
        for (LocationModel locationModel: arrLocationModels) {
            uploadLocation(locationModel);
        }
    }

    private void uploadLocation(LocationModel locationModel) {
        try {
            if (NetworkUtility.getInstance().isNetworkAvailable()) {
                String url = Consts.SEVER_ADDRESS + "/api/locationapi/?usertoken=" + User.getUserToken()
                        + "&lat=" + locationModel.location.getLatitude()
                        + "&lng=" + locationModel.location.getLongitude()
                        + "&locationdate=" + Uri.encode(locationModel.getDateTimeString());

                Log.e(TAG, "upload location url: " + url);

                Future<String> string = Ion.with(this).load("post", url).asString();
                Log.e(TAG, "upload location result: " + string.get());
                if (string.get().equals("true")) {
                    LocationModel.deleteRecord(locationModel);
                }
            }
        } catch (Exception e) {

        }
    }

    private void stopTimer() {
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        stopTimer();
        super.onDestroy();
    }
}