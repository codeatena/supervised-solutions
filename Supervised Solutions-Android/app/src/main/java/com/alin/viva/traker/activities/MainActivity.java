package com.alin.viva.traker.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.alin.viva.traker.Consts;
import com.alin.viva.traker.R;
import com.alin.viva.traker.model.User;
import com.alin.viva.traker.services.LocationService;
import com.alin.viva.traker.services.TimerService;

import alin.lib.utility.DialogUtility;
import alin.lib.utility.LocationUtility;
import alin.lib.utility.PreferenceUtility;
import alin.lib.widget.DefaultWebClient;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        webView = (WebView) findViewById(R.id.webView);
    }

    private void initValue() {

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new DefaultWebClient(this));

        String url = Consts.SEVER_ADDRESS + "/account/applogin?token=" + User.getUserToken()
                + "&systemapptoken=ZDgxNmEzMDUtNzZjMC00NzZjLWIyYjYtMzQwOWRhMGFlNzBm";

        Log.e(TAG, "main url: " + url);
        webView.loadUrl(url);
    }

    private void initEvent() {
        if (!LocationUtility.getInstance().isEnableLocation()) {
            DialogInterface.OnClickListener eventOnPositive = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            };
            DialogInterface.OnClickListener eventOnNegative = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            };
            Dialog dialog = DialogUtility.getInstance().createDialog(this, R.string.information, R.string.sure_turn_on_gps, eventOnPositive, eventOnNegative);
            dialog.show();
        }

        startService(new Intent(this, LocationService.class));
        startService(new Intent(this, TimerService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_logout) {

            DialogInterface.OnClickListener eventOnPositive = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            };
            DialogInterface.OnClickListener eventOnNegative = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    logout();
                }
            };

            Dialog dialog = DialogUtility.getInstance().createDialog(this, R.string.information, R.string.sure_logout, eventOnPositive, eventOnNegative);
            dialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        stopService(new Intent(this, LocationService.class));
        stopService(new Intent(this, TimerService.class));
        User.setUserToken("");
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (webView.canGoBack()) {
            webView.goBack();
        }
    }
}