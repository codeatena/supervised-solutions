package alin.lib.utility;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.alin.viva.traker.App;

import java.util.ArrayList;
import java.util.List;

public class LocationUtility {

    final static String TAG = "LocationUtility";

    public static LocationUtility instance;

    public static LocationUtility getInstance() {
        if (instance == null) {
            instance = new LocationUtility();
        }
        return instance;
    }

    public String getLatLng(Location currentLocation) {
        if (currentLocation != null) {
            return currentLocation.getLatitude() + ", " + currentLocation.getLongitude();
        } else {
            return "Null";
        }
    }
    
    public double getDistance(double latA, double lngA, double latB, double lngB) {
    	double distance;

    	Location locationA = createLocation(latA, lngA);
    	Location locationB = createLocation(latB, lngB);

    	distance = locationA.distanceTo(locationB);
    	
    	return distance;
    }

    public boolean isEnableLocationByGPS() {
        LocationManager locationManager = (LocationManager) App.getInstance().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public boolean isEnableLocationByNetwork() {
        LocationManager locationManager = (LocationManager) App.getInstance().getSystemService(Context.LOCATION_SERVICE);
        boolean isEnableLocationByNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//        boolean isNetworkAvailable = NetworkUtility.getInstance().isNetworkAvailable();
        return isEnableLocationByNetwork;
    }

    public boolean isEnableLocation() {
        if (isEnableLocationByGPS() || isEnableLocationByNetwork()) {
            return true;
        }
        return false;
    }

    public void setCurrentLocation(Location location) {
        PreferenceUtility.getInstance().prefEditor.putFloat("current_lat", (float) location.getLatitude());
        PreferenceUtility.getInstance().prefEditor.putFloat("current_lng", (float) location.getLongitude());
        PreferenceUtility.getInstance().prefEditor.commit();
    }

    public Location getCurrentLocation() {
        Location location = createLocation((double) PreferenceUtility.getInstance().sharedPreferences.getFloat("current_lat", 0.0f),
                (double) PreferenceUtility.getInstance().sharedPreferences.getFloat("current_lng", 0.0f));
        return location;
    }

    public Location createLocation(double lat, double lng) {
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(lng);
        return location;
    }
}